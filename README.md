# Think Trello


## Requirement to run in Local(without docker)
- Node.js >= 16 (You can use nvm to install multiple versions)
- Mysql server

## Install depencendcies
- npm install (If got an error please adding --force)

## Set up environment variables in .env
- db_host = your db host
- db_port = your mysql db port
- db_user = your db username
- db_password = your db password
- db_name = your db name for dev env
- db_test_name = your db name for test env
- port = 3000
- NODE_ENV = (dev or test)
- *Please create databases for dev and test before running

## Running service(DEV mode)
If found an error please following "Transpile and running as js file" step
```
npm run dev
```

## Transpile and running as js file
```
npm run build
```
then
```
npm run start
```
## Running test
Please start the server and then run the following command in the other terminal
```
npm run test
```

## Reference
- Development : https://www.youtube.com/watch?v=fov5e6XJgwc&ab_channel=PedroTech
- Unit test : https://dev.to/neshaz/testing-graphql-server-in-nodejs-55cm

FROM node:16-alpine

LABEL maintainer=PittawatPhongboribun 

WORKDIR /app

#Copy package.json and package-lock.json to /app
COPY package*.json tsconfig.json .env ./

#Clean and intall dependencies
RUN npm install

#Copy source file to /app/src
COPY ./src ./src

#Compile Typescript to Javascript
RUN npm run build

# set application PORT and expose docker PORT, 8080 for monster-service
ENV PORT 3000
EXPOSE ${PORT}

CMD [ "npm", "run", "start" ]
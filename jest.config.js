module.exports = {
    roots: ['<rootDir>/src'],
    transform: {
        ".ts": "ts-jest"
    },
    coveragePathIgnorePatterns: [
        "<rootDir>/node_modules", "<rootDir>/src/Entity", "<rootDir>/src/Schema/TypeDef", "<rootDir>/src/db"
    ],
    coverageReporters: ['json', 'lcov', 'text', 'clover']
}
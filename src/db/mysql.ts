import { DataSource } from "typeorm";
import { List } from "../Entity/list";
import { Task } from "../Entity/task";

export const MysqlDataSource = (config: any) => new DataSource({
    type: "mysql",
    host: config.host,
    port: config.db_port,
    username: config.db_user,
    password: config.db_password,
    database: config.db_name,
    entities: [Task, List,],
    synchronize: true,
    logging: true,
});

export const MysqlDataSourceForTest = (config: any) => new DataSource({
    type: "mysql",
    host: config.host,
    port: config.db_port,
    username: config.db_user,
    password: config.db_password,
    database: config.db_name,
    entities: [Task, List,],
    synchronize: true,
    logging: false,
    dropSchema: true,
});
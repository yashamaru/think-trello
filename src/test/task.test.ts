import request from 'supertest';
import { baseURL, queryGetAllTasks } from './utils';

describe('Tests the createList Mutation', () => {

  it('should successfully get empty tasks', async () => {

    request(baseURL).post('').send(queryGetAllTasks).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllTasks).toBeTruthy();
        expect(response.body.data.getAllTasks.length).toEqual(0);
    });
  })

  it('should successfully create a task into the list id 1', async () => {
    const createAListCommand = {"query":"mutation {\r\n  createList(title: \"back-end\") {\r\n    id\r\n    title\r\n  }\r\n}","variables":{}};
    const command = {"query":"mutation{\r\n  createTask(title: \"task1\", listId: 1) {\r\n    id\r\n    title\r\n    status\r\n  }\r\n}","variables":{}};

    await request(baseURL).post('').send(createAListCommand).expect(200)
    await request(baseURL).post('').send(command).expect(200)
    request(baseURL).post('').send(queryGetAllTasks).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllTasks).toBeTruthy();
        expect(response.body.data.getAllTasks.length).toEqual(1);
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task1')).title
        ).toBeTruthy();
    });
  })

  it('should successfully create a task2 into the list id 1', async () => {
    const command = {"query":"mutation{\r\n  createTask(title: \"task2\", listId: 1) {\r\n    id\r\n    title\r\n    status\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command).expect(200);
    request(baseURL).post('').send(queryGetAllTasks).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllTasks).toBeTruthy();
        expect(response.body.data.getAllTasks.length).toEqual(2);
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task2')).title
        ).toBeTruthy();
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task2')).position
        ).toEqual(2);
    });
  })

  it('should successfully update position of task2 to 1, task1 will drop to 2 automatically', async () => {
    const command = {"query":"mutation{\r\n  updateTask(id: 2, title: \"task2\", status: \"todo\", position: 1) {\r\n    id\r\n    title\r\n    status\r\n    position\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command).expect(200);
    request(baseURL).post('').send(queryGetAllTasks).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllTasks).toBeTruthy();
        expect(response.body.data.getAllTasks.length).toEqual(2);
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task2')).position
        ).toEqual(1);
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task1')).position
        ).toEqual(2);
    });
  })

  it('should delete task1 successfully', async () => {
    const command = {"query":"mutation{\r\n  deleteTask(id: 1) {\r\n      id\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command).expect(200);
    request(baseURL).post('').send(queryGetAllTasks).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllTasks).toBeTruthy();
        expect(response.body.data.getAllTasks.length).toEqual(1);
        expect(
            (response.body.data.getAllTasks.array.filter((element: { title: string; }) => element.title === 'task1'))
        ).toBeNull();
    });
  })
})
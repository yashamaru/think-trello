import request from 'supertest';
import { baseURL, queryGetAllLists } from './utils';

describe('Tests the createList Mutation', () => {

  it('should successfully get lists', async () => {

    request(baseURL).post('').send(queryGetAllLists).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        expect(response.body.data).toBeTruthy();
        expect(response.body.data.getAllLists).toBeTruthy();
        expect(response.body.data.getAllLists.length).toEqual(0);
    });
  })

  it('Test create a new list successfully', async () => {
    const command = {"query":"mutation {\r\n  createList(title: \"back-end\") {\r\n    id\r\n    title\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command).expect(200)

    request(baseURL).post('').send(queryGetAllLists).expect(200).end(async function(error, response){
        if ( error ) console.log(error);

        const lastIndex: number = response.body.data.getAllLists.length - 1;
        expect(response.body.data.getAllLists[lastIndex].title).toEqual('back-end');
    });
  })

  it('Test update a list successfully', async () => {
    const command = {"query":"mutation{\r\n  updateList(id: 1, title: \"front-end\") {\r\n    id\r\n    title\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command).expect(200)

    request(baseURL).post('').send(queryGetAllLists).expect(200).end(async function(error, response){
      if ( error ) console.log(error);

      expect(
        (response.body.data.getAllLists.array.filter((element: { id: number; }) => element.id === 1)).title
      ).toEqual('front-end');
    });
  })

  it('Test delete a new list successfully', async () => {
    const command = {"query":"mutation{\r\n  deleteList(id: 1) {\r\n    id\r\n  }\r\n}","variables":{}};

    request(baseURL).post('').send(command)

    request(baseURL).post('').send(queryGetAllLists).expect(200).end(async function(error, response){
      if ( error ) console.log(error);

      expect(response.body.data).toBeTruthy();
      expect(response.body.data.getAllLists).toBeTruthy();
      expect(response.body.data.getAllLists.length).toEqual(0);
    });
  })
})
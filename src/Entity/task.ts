import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { List } from './list';

@Entity()
export class Task extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        length: 16,
    })
    title!: string

    @Column({
        length: 8,
        default: 'Todo'
    })
    status!: string

    @Column()
    position!: number

    @ManyToOne(() => List, list => list.tasks, {cascade: true})
    list!: Promise<List>;

    @Column()
    listId!: string
}
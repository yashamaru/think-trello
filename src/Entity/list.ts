import { BaseEntity, Column, Entity, JoinTable, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Task } from './task';

@Entity()
export class List extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number

    @Column({
        length: 16,
    })
    title!: string

    @OneToMany(() => Task, task => task.list)
    tasks!: Promise<Task[]>;
}
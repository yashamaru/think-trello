import express, { Express } from 'express';
import { graphqlHTTP } from 'express-graphql';
import cors from 'cors';
import dotenv from 'dotenv';
import { MysqlDataSource, MysqlDataSourceForTest } from './db/mysql';
import { schema } from './Schema/index';

export const main = async() => {
    dotenv.config();
    const port: number = Number(process.env.port) || 3000;

    if (process.env.NODE_ENV === "test") {
        MysqlDataSourceForTest({
            host: process.env.db_host,
            db_port: process.env.db_port,
            db_user: process.env.db_user,
            db_password: process.env.db_password,
            db_name: process.env.db_test_name
        }).initialize().catch((err: any) => {
            console.error(`Got an error when connect to the database : ${String(err)}`);
        });
    } else {
        MysqlDataSource({
            host: process.env.db_host,
            db_port: process.env.db_port,
            db_user: process.env.db_user,
            db_password: process.env.db_password,
            db_name: process.env.db_name
        }).initialize().catch((err: any) => {
            console.error(`Got an error when connect to the database : ${String(err)}`);
        });
    }

    const app: Express = express();
    app.use(cors());
    app.use(express.json());
    app.use("/graphql", graphqlHTTP({
        schema,
        graphiql: true
    }));

    app.listen(port, () => {
        console.log(`The server is running on port: ${port}`);
    })
}

main().catch(err => {
    console.error(`Error on running main:`, String(err));
})
import { GraphQLObjectType, GraphQLSchema } from "graphql";
import { GET_ALL_LISTS } from './Query/List';
import { CREATE_LIST, DELETE_LIST, UPDATE_LIST } from './Mutation/List';
import { GET_ALL_TASKS } from './Query/Task';
import { CREATE_TASK, DELETE_TASK, UPDATE_TASK } from './Mutation/Task';

const rootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        getAllLists: GET_ALL_LISTS,
        getAllTasks: GET_ALL_TASKS,
    },
});

const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createList: CREATE_LIST,
        updateList: UPDATE_LIST,
        deleteList: DELETE_LIST,
        createTask: CREATE_TASK,
        updateTask: UPDATE_TASK,
        deleteTask: DELETE_TASK,
    }
});

export const schema = new GraphQLSchema({
    query: rootQuery,
    mutation: mutation
});
import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLList } from 'graphql';
import { TaskType } from './Task';

export const ListType = new GraphQLObjectType({
    name: 'List',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        tasks: { type: new GraphQLList(TaskType) }
    })
});
import { GraphQLObjectType, GraphQLID, GraphQLInt, GraphQLString } from 'graphql';

export const TaskType = new GraphQLObjectType({
    name: 'Task',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        status: { type: GraphQLString },
        position: { type: GraphQLInt },
        listId: { type: GraphQLInt },
    })
});
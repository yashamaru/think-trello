import { GraphQLList } from 'graphql';
import { Task } from '../../Entity/task';
import { TaskType } from './../TypeDef/Task';

export const GET_ALL_TASKS = {
    type: new GraphQLList(TaskType),
    async resolve(): Promise<any> {
        return await Task.find();
    }
};
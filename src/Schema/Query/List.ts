import { GraphQLList } from 'graphql';
import { List } from '../../Entity/list';
import { ListType } from './../TypeDef/List';
import { Task } from '../../Entity/task';

export const GET_ALL_LISTS = {
    type: new GraphQLList(ListType),
    async resolve(): Promise<any> {
        const tasks: Task[] = await Task.find({
            order: {
                listId: "ASC",
                position: "ASC"
            }
        });
        const lists: any[] = await List.find();

        // console.log(tasks.length);

        for(let i=0; i < lists.length; i++) {
            const tempTasks: any[] = [];

            for(let j=0; j < tasks.length; j++) {
                if ((await tasks[j].list).id == lists[i].id) {
                    tempTasks.push(tasks[j]);
                    // tempTasks.push(tasks.splice(j, 1));
                }
            }

            lists[i]["tasks"] = tempTasks;
        }

        return lists
    }
};
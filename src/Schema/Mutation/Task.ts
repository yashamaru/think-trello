import { GraphQLInt, GraphQLList, GraphQLString } from 'graphql';
import { Task } from '../../Entity/task';
import { TaskType } from './../TypeDef/Task';

export const CREATE_TASK = {
    type: TaskType,
    args: {
        title: { type: GraphQLString },
        listId: { type: GraphQLInt },
    },
    async resolve(parent: any, args: any): Promise<any> {
        const {title, listId} = args;

        try {
            const maxPosition = await Task.query(`SELECT MAX(position) FROM task WHERE listId = ?;`, [listId]);
            const position: number = maxPosition[0]['MAX(position)'] + 1;
            await Task.insert({ title, position, listId });
        } catch(error: any) {
            console.error(`Cannot insert task: ${String(args)}, error:`, String(error));
            return null;
        }
        
        return args;
    }
};

export const UPDATE_TASK = {
    type: TaskType,
    args: {
        id: { type: GraphQLInt },
        title: { type: GraphQLString },
        status: { type: GraphQLString },
        position: { type: GraphQLInt },
    },
    async resolve(parent: any, args: any): Promise<any> {
        const {title, status, position} = args;

        try {
            const task: Task | null = await Task.findOneBy({id: args.id});

            if (task === null || position === 0) return null;
            if (task.position !== position) await sort_task(position, task);
    
            await Task.update(args.id, { title, status, position });

        } catch(error: any) {
            console.error(`Cannot update task: ${String(args)}, error:`, String(error));
            return null;
        }

        return args;
    }
};

export const DELETE_TASK = {
    type: TaskType,
    args: {
        id: { type: GraphQLInt }
    },
    async resolve(parent: any, args: any): Promise<any> {
        try {
            const task: Task | null = await Task.findOneBy({id: args.id});
            if (task === null) return null;
    
            await Task.delete(args.id);
            await sort_task(task.position, task)

        } catch(error: any) {
            console.error(`Cannot delete task: ${String(args.id)}, error:`, String(error));
        }

        return null;
    }
};

const sort_task = async (newPosition: number, currentTask: Task) => {
    try {
        const tasks: Task[] = await Task.find({
            where: [
                { listId: currentTask.listId }
            ],
            order: {
                position: "ASC"
            }
        });
    
        if (tasks.length === 1) return;
    
        if (currentTask.position > newPosition) {
            // Move the others down
            await moveTasksDown(tasks, currentTask);
        } else {
            // currentTask.position =< newPosition
            // Move the others up
            await moveTasksUp(tasks, currentTask);
        }
    } catch(error) {
        console.error(`Cannot sort task, error:`, String(error));
        throw error;
    }
};

const moveTasksDown = async (tasks: Task[], currentTask: Task) => {
    for(let i=0; i < tasks.length; i++) {
        if (tasks[i].position > currentTask.position) break;
        else if (tasks[i].position < currentTask.position) {
            tasks[i].position += 1;
            await tasks[i].save();
        }
    }
}

const moveTasksUp = async (tasks: Task[], currentTask: Task) => {
    for(let i=0; i < tasks.length; i++) {
        if (tasks[i].position < currentTask.position) break;
        else if (tasks[i].position > currentTask.position) {
            tasks[i].position -= 1;
            await tasks[i].save();
        }
    }
}
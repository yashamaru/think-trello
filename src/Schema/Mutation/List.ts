import { GraphQLInt, GraphQLString } from 'graphql';
import { List } from '../../Entity/list';
import { ListType } from './../TypeDef/List';

export const CREATE_LIST = {
    type: ListType,
    args: {
        title: {type: GraphQLString}
    },
    async resolve(parent: any, args: any): Promise<any> {
        const { title } = args;
        await List.insert({ title });
        return args;
    }
};

export const UPDATE_LIST = {
    type: ListType,
    args: {
        id: {type: GraphQLInt},
        title: {type: GraphQLString}
    },
    async resolve(parent: any, args: any): Promise<any> {
        const { id, title } = args;

        try {
            const list: List | null = await List.findOneBy({id: args.id});

            if (list === null) return null;
    
            await List.update(args.id, { title });

        } catch(error: any) {
            console.error(`Cannot update list: ${String(args)}, error:`, String(error));
            return null;
        }

        return args;
    }
};

export const DELETE_LIST = {
    type: ListType,
    args: {
        id: {type: GraphQLInt}
    },
    async resolve(parent: any, args: any): Promise<any> {
        const { id } = args;
        await List.delete({ id });
        return args;
    }
};